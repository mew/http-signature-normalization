use std::time::SystemTime;

use http_signature_normalization_reqwest::prelude::*;
use httpdate::HttpDate;
use reqwest::{header::DATE, Client};
use sha2::{Digest, Sha256};

async fn request(config: Config) -> Result<(), Box<dyn std::error::Error>> {
    let digest = Sha256::new();

    let client = Client::new();

    let request = client
        .post("http://127.0.0.1:8010/")
        .header("User-Agent", "Reqwest")
        .header("Accept", "text/plain")
        .header(DATE, HttpDate::from(SystemTime::now()).to_string())
        .signature_with_digest(config, "my-key-id", digest, "Hewwo-owo", |s| {
            println!("Signing String\n{}", s);
            Ok(base64::encode(s)) as Result<_, MyError>
        })
        .await?;

    let body = client.execute(request).await?.bytes().await?;

    println!("{:?}", body);
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    std::env::set_var("RUST_LOG", "info");
    pretty_env_logger::init();

    let config = Config::default().require_header("accept");

    request(config.clone()).await?;
    request(config.mastodon_compat()).await?;
    Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum MyError {
    #[error("Failed to create signing string, {0}")]
    Convert(#[from] SignError),

    #[error("Failed to send request")]
    SendRequest(#[from] reqwest::Error),

    #[error("Failed to retrieve request body")]
    Body(reqwest::Error),
}
