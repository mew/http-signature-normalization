# HTTP Signature Normaliztion Reqwest
_An HTTP Signatures library that leaves the signing to you_

- [crates.io](https://crates.io/crates/http-signature-normalization-reqwest)
- [docs.rs](https://docs.rs/http-signature-normalization-reqwest)
- [Hit me up on Mastodon](https://asonix.dog/@asonix)

Http Signature Normalization is a minimal-dependency crate for producing HTTP Signatures with user-provided signing and verification. The API is simple; there's a series of steps for creation and verification with types that ensure reasonable usage.

## Usage

This crate provides extensions the RequestBuilder type from reqwest

#### First, add this crate to your dependencies
```toml
http-signature-normalization-reqwest = { version = "0.2.0", default-features = false, features = ["sha-2"] }
reqwest = "0.11"
sha2 = "0.9"
thiserror = "0.1"
tokio = "1"
```

#### Then, use it in your client

```rust
use http_signature_normalization_reqwest::prelude::*;
use reqwest::{header::DATE, Client};
use sha2::{Digest, Sha256};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = Config::default().require_header("accept");

    let digest = Sha256::new();

    let response = Client::new()
        .post("http://127.0.0.1:8010/")
        .header("User-Agent", "Reqwest")
        .header("Accept", "text/plain")
        .signature_with_digest(config, "my-key-id", digest, "my request body", |s| {
            println!("Signing String\n{}", s);
            Ok(base64::encode(s)) as Result<_, MyError>
        })
        .await?;

    let body = response.bytes().await.map_err(MyError::Body)?;

    println!("{:?}", body);
    Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum MyError {
    #[error("Failed to create signing string, {0}")]
    Convert(#[from] SignError),

    #[error("Failed to send request")]
    SendRequest(#[from] reqwest::Error),

    #[error("Failed to retrieve request body")]
    Body(reqwest::Error),
}
```

### Contributing
Unless otherwise stated, all contributions to this project will be licensed under the CSL with
the exceptions listed in the License section of this file.

### License
This work is licensed under the Cooperative Software License. This is not a Free Software
License, but may be considered a "source-available License." For most hobbyists, self-employed
developers, worker-owned companies, and cooperatives, this software can be used in most
projects so long as this software is distributed under the terms of the CSL. For more
information, see the provided LICENSE file. If none exists, the license can be found online
[here](https://lynnesbian.space/csl/). If you are a free software project and wish to use this
software under the terms of the GNU Affero General Public License, please contact me at
[asonix@asonix.dog](mailto:asonix@asonix.dog) and we can sort that out. If you wish to use this
project under any other license, especially in proprietary software, the answer is likely no.

Http Signature Normalization Reqwest is currently licensed under the AGPL to the Lemmy project, found
at [github.com/LemmyNet/lemmy](https://github.com/LemmyNet/lemmy)

