//! Types used for signing a request
use http::header::{HeaderMap, HeaderName, HeaderValue, InvalidHeaderValue, AUTHORIZATION};

/// A thin wrapper around the base library's Signed type
pub struct Signed {
    /// The inner Signed type which can produce string versions of HTTP headers
    pub signed: http_signature_normalization::create::Signed,
}

/// A thin wrapper around the base library's Unsigned type
///
/// This is used to produce a Signed type that can interact with http's types
pub struct Unsigned {
    /// The inner Unsigned type, which can produce the base library's Signed type
    pub unsigned: http_signature_normalization::create::Unsigned,
}

impl Signed {
    /// Set the Signature Header in a given HeaderMap
    pub fn signature_header(self, hm: &mut HeaderMap) -> Result<(), InvalidHeaderValue> {
        hm.insert(
            HeaderName::from_static("Signature"),
            HeaderValue::from_str(&self.signed.signature_header())?,
        );

        Ok(())
    }

    /// Set the Authorization Header in a given HeaderMap
    pub fn authorization_header(self, hm: &mut HeaderMap) -> Result<(), InvalidHeaderValue> {
        hm.insert(
            AUTHORIZATION,
            HeaderValue::from_str(&self.signed.authorization_header())?,
        );
        Ok(())
    }
}

impl Unsigned {
    /// Sign the request
    ///
    /// ```rust,ignore
    /// let signed = unsigned.sign("my-key-id".to_owned(), |signing_string| {
    ///     let signature = private_key.sign(signing_string)?;
    ///     Ok(base64::encode(signature))
    /// })?;
    /// ```
    pub fn sign<F, E>(self, key_id: String, f: F) -> Result<Signed, E>
    where
        F: FnOnce(&str) -> Result<String, E>,
    {
        let signed = self.unsigned.sign(key_id, f)?;
        Ok(Signed { signed })
    }
}
