# HTTP Signature Normaliztion
_An HTTP Signatures library that leaves the signing to you_

- [crates.io](https://crates.io/crates/http-signature-normalization)
- [docs.rs](https://docs.rs/http-signature-normalization)
- [Hit me up on Mastodon](https://asonix.dog/@asonix)

Http Signature Normalization is a minimal-dependency crate for producing HTTP Signatures with user-provided signing and verification. The API is simple; there's a series of steps for creation and verification with types that ensure reasonable usage.

```rust
use chrono::Duration;
use http_signature_normalization::Config;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = Config {
        expires_after: Duation::secs(5),
    };

    let headers = BTreeMap::new();

    let signature_header_value = config
        .begin_sign("GET", "/foo?bar=baz", headers)
        .sign("my-key-id".to_owned(), |signing_string| {
            // sign the string here
            Ok(signing_string.to_owned()) as Result<_, Box<dyn std::error::Error>>
        })?
        .signature_header();

    let mut headers = BTreeMap::new();
    headers.insert("Signature".to_owned(), signature_header_value);

    let verified = config
        .begin_verify("GET", "/foo?bar=baz", headers)?
        .verify(|sig, signing_string| {
            // Verify the signature here
            sig == signing_string
        });

    assert!(verified)
}
```

### Contributing
Unless otherwise stated, all contributions to this project will be licensed under the CSL with
the exceptions listed in the License section of this file.

### License
This work is licensed under the Cooperative Software License. This is not a Free Software
License, but may be considered a "source-available License." For most hobbyists, self-employed
developers, worker-owned companies, and cooperatives, this software can be used in most
projects so long as this software is distributed under the terms of the CSL. For more
information, see the provided LICENSE file. If none exists, the license can be found online
[here](https://lynnesbian.space/csl/). If you are a free software project and wish to use this
software under the terms of the GNU Affero General Public License, please contact me at
[asonix@asonix.dog](mailto:asonix@asonix.dog) and we can sort that out. If you wish to use this
project under any other license, especially in proprietary software, the answer is likely no.

Http Signature Normalization is currently licensed under the AGPL to the Lemmy project, found
at [github.com/LemmyNet/lemmy](https://github.com/LemmyNet/lemmy)
