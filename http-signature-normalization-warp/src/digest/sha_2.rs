use super::{DigestPart, DigestVerify};
use sha2::Digest;

fn verify<D>(verifier: &mut D, parts: &[DigestPart], payload: &[u8], name: &str) -> bool
where
    D: Digest,
{
    let part = if let Some(part) = parts.iter().find(|p| p.algorithm == name) {
        part
    } else {
        return false;
    };

    verifier.input(payload);
    let computed = base64::encode(&verifier.result_reset());

    computed == part.digest
}

impl DigestVerify for sha2::Sha256 {
    fn verify(&mut self, digests: &[DigestPart], payload: &[u8]) -> bool {
        verify(self, digests, payload, "sha-256")
    }
}

impl DigestVerify for sha2::Sha512 {
    fn verify(&mut self, digests: &[DigestPart], payload: &[u8]) -> bool {
        verify(self, digests, payload, "sha-512")
    }
}
